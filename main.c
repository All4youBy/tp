//
//  main.c
//  Lab_4
//
//  Created by Maxim  on 18.03.17.
//  Copyright © 2017 Maxim . All rights reserved.
//

#include <stdio.h>
#include <sqlite3.h>

static int callBack(void *NotUsed,int argc,char **argv,char **azColName)
{
    int i;
    const char* data = "Callback function called";
    fprintf(stderr, "%s: ", (const char*)data);
    for(i = 0;i < argc;i++)
    {
        printf("%s = %s\n",azColName[i],argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}

int main(int argc, const char * argv[]) {
    
    sqlite3 *dataBase;
    int rc;
    const char* data = "Callback function called";
    
    rc = sqlite3_open("test.db",&dataBase);
    
    if(rc)
    {
        fprintf(stderr,"Canno't open database: %s\n",sqlite3_errmsg(dataBase));
        return 0;
    }
    else
    {
        fprintf(stderr, "Database is open.\n");
    }
    
    char* zErrMesg = 0;
    char*sql = "DELETE from COMPANY where ID=2; " \
    "SELECT * from COMPANY";
    
    rc = sqlite3_exec(dataBase, sql, callBack,(void*)data,&zErrMesg);
    
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "SQL error: %s\n", zErrMesg);
        sqlite3_free(zErrMesg);
    }else
    {
        fprintf(stdout, "Operation done successfully\n");
    }
    
    sqlite3_close(dataBase);
    return 0;
}


